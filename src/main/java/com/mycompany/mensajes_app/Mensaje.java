/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

/**
 * POJO Mensaje
 * @author Romina
 */
public class Mensaje {
    /**
     * Identificador del mensaje
     */
    private int idMensaje;
    /**
     * Mensaje
     */
    private String mensaje;
    /**
     * Autor del mensaje
     */
    private String autorMensaje;
    /**
     * Fecha del mensaje
     */
    private String fechaMensaje;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public int getIdMensaje() {
        return idMensaje;
    }
    
    public void setIdMensaje(int idMensaje) {
        this.idMensaje = idMensaje;
    }
    
    public String getMensaje() {
        return mensaje;
    }
    
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    public String getAutorMensaje() {
        return autorMensaje;
    }
    
    public void setAutorMensaje(String autorMensaje) {
        this.autorMensaje = autorMensaje;
    }
    
    public String getFechaMensaje() {
        return fechaMensaje;
    }
    
    public void setFechaMensaje(String fechaMensaje) {
        this.fechaMensaje = fechaMensaje;
    }
//</editor-fold>

    
    /**
     * Constructor
     */
    public Mensaje() {
    }

    
    /**
     * Constructor con parámetros
     * @param mensaje
     * @param autorMensaje
     * @param fechaMensaje 
     */
    public Mensaje(String mensaje, String autorMensaje, String fechaMensaje) {
        this.mensaje = mensaje;
        this.autorMensaje = autorMensaje;
        this.fechaMensaje = fechaMensaje;
    }
    
    
    
    
    
    
    
}
