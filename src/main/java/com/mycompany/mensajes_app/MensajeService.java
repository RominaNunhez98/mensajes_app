/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.util.Scanner;

/**
 *
 * @author Romina
 */
public class MensajeService {

    /**
     * Servicio para crear mensaje
     */
    public static void crearMensaje() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu mensaje");
        String mensaje = sc.nextLine();

        System.out.println("Tu nombre");
        String nombre = sc.nextLine();

        Mensaje registroMensaje = new Mensaje();
        registroMensaje.setMensaje(mensaje);
        registroMensaje.setAutorMensaje(nombre);

        MensajeDAO.crearMensajeDB(registroMensaje);
    }
    
    /**
     * Servicio para listar mensaje
     */
    public static void listarMensaje() {

        MensajeDAO.leerMensajeDB();

    }

    /**
     * Servicio para borrar mensaje
     */
    public static void borrarMensaje() {
        Scanner sc = new Scanner(System.in);
        System.out.println("El ID del mensaje a borrar");
        int idMensaje = sc.nextInt();

        MensajeDAO.borrarMensajeDB(idMensaje);

    }

    /**
     * Servicio para editar mensaje
     */
    public static void editarMensaje() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu nuevo mensaje: ");
        String mensajeActualizado = sc.nextLine();
        System.out.println("Indique el ID del mensaje a editar: ");
        int idMensaje = sc.nextInt();
        
        Mensaje mensajeEditado = new Mensaje();
        
        mensajeEditado.setMensaje(mensajeActualizado);
        mensajeEditado.setIdMensaje(idMensaje);
        
        MensajeDAO.actualizarMensajeDB(mensajeEditado);

    }

}
